<!DOCTYPE html>
<html>

<body>

    <form action="<?=base_url('Practice/insertData')?>" method="POST" role="form">
        <legend>INSERT FORM</legend>

        <div class="form-group">
            <label for="">Name</label>
            <input type="text" class="form-control" id="name" name='name' placeholder="Input field">
        </div>
        <div class="form-group">
            <label for="">Email</label>
            <input type="text" class="form-control" id="email" name='email' placeholder="Input field">
        </div>
        <div class="form-group">
            <label for="">Branch</label>
            <input type="text" class="form-control" id="branch" name='branch' placeholder="Input field">
        </div>



        <button type="submit" class="btn btn-primary">SAVE</button>
    </form>

</body>

</html>