<!DOCTYPE html>
<html>

<head>
    <a href="<?= base_url('Dashboard/insertForm') ?>" button type="submit" class="btn btn-primary">ADD </button></a>

</head>

<body>
    <table class="table table-condensed table-hover">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Branch</th>
                <th>Actions</th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach($r as $a) { ?>
                <tr>
                    <td><?= $a['name']; ?></td>
                    <td><?= $a['email']; ?></td>
                    <td><?= $a['branch']; ?></td>
                    <td>
                        <a href="<?= base_url('') ?>" button type="submit" class="btn btn-primary">EDIT </button></a>
                        <a href="<?= base_url('') ?>" button type="submit" class="btn btn-primary">DELETE </button></a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

</body>

</html>