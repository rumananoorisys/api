<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div class="row">
<a href="<?=base_url('Practice/insertForm')?>" class="btn btn-info btn-lg" style="margin-left: 50px;">
          <span class="glyphicon glyphicon-plus"> Add</span>
          <!-- <i class="fa fa-smile"></i> -->
        </a>
</div>


    <body>
        
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Branch</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($r as $a){?>
                <tr>
                    <td><i class="fa fa-user"></i>    <?=$a['name']?></td>
                    <td><i class="fa fa-envelope-open"></i>    <?=$a['email']?></td>
                    <td><i class="fa fa-bank"></i>    <?=$a['branch']?></td>
                    <td>
                        <a href="<?=base_url('Practice/editData/').$a['id']?>"> <i class="fa fa-edit" style="color: green;"></i></a>
                        <a href="<?=base_url('Practice/deleteData/').$a['id']?>"> <i class="fa fa-trash" style="color: red;"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        
    </body>
</html>