

<!DOCTYPE html>
<html>
    <body>
    <form action="<?=base_url('Practice/updateRecord/').$r['id']?>" method="POST" role="form">
        <legend>EDIT FORM</legend>

        <div class="form-group">
            <label for="">Name</label>
            <input type="text" class="form-control" id="name" name='name' value = "<?=$r['name']?>"  placeholder="Input field">
        </div>
        <div class="form-group">
            <label for="">Email</label>
            <input type="text" class="form-control" id="email" name='email'  value = "<?=$r['email']?>"  placeholder="Input field">
        </div>
        <div class="form-group">
            <label for="">Branch</label>
            <input type="text" class="form-control" id="branch" name='branch' value = "<?=$r['branch']?>" placeholder="Input field">
        </div>



        <button type="submit" class="btn btn-primary" >SUBMIT</button>
    </form>
    </body>
</html>