<!DOCTYPE html>
<html>

<body>

    <form action="<?=base_url('Practice/getData')?>" method="POST" class="form-horizontal" role="form">
        <div class="form-group">
            <legend>LOGIN FORM</legend>
        </div>
        <div class="form-group">
            <label for="">Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Input field">
        </div>
        <div class="form-group">
            <label for="">Email</label>
            <input type="text" class="form-control" id="email" name="email"  placeholder="Input field">
        </div>
        <div class="form-group">
            <label for="">Branch</label>
            <input type="text" class="form-control" id="branch"  name="branch" placeholder="Input field">
        </div>


        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <button type="submit" class="btn btn-primary">LOGIN</button>
            </div>
        </div>
    </form>

</body>

</html>