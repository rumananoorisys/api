<?php
class Practice extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Practice_model','p');
    }

    public function showForm()
    {
        $this->load->view('Practiceform_view');
    }

    public function getData()
    {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $branch = $this->input->post('branch');

        $data = [
            'name' => $name,
            'email' => $email,
            'branch' => $branch
        ];

        $res = $this->p->checkInfo($data);

        if($res)
        {
            // echo "success";
            redirect('table');
        }else
        {
            echo "error";
        }
    }

    public function showTable()
    {
        $res['r'] = $this->p->getTable();

        $this->load->view('PracticeTable_view',$res);
    }

    public function insertForm()
    {
        $this->load->view('InsertForm_view');

    }

    public function insertData()
    {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $branch = $this->input->post('branch');

        $data = [
            'name'=>$name,
            'email'=>$email,
            'branch'=>$branch
        ];

        $res = $this->p->getData($data);

        if($res)
        {
            redirect('table');
        }else
        {
            echo "Error";
        }
    }

    public function deleteData($id)
    {   
        $res = $this->p->deletRecord($id);
    
        if($res)
        {
            //  echo "Deleted";
            redirect('table');
        }else 
        {
            echo "Try Again.";
        }
    }

    public function editData($id)
    {
        
        $res['r'] = $this->p->editRecord($id);
        // echo json_encode($res);exit;
        $this->load->view('editform_view',$res);
    }

    public function updateRecord($id)
    {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $branch = $this->input->post('branch');

        $data = [
            'name'=>$name,
            'email'=>$email,
            'branch'=>$branch
        ];

        $res = $this->p->updateData($data,$id);
        if($res)
        {
            // echo "updated";
            redirect('table');

        }else
        {
            echo "Error";
        }
    }



}


?>