<?php
class Login extends CI_Controller
{    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model','l');
    }

    public function display()
    {
        $this->load->view('LoginForm_view');
    }

    public function getData()
    {
        // echo "hi"; die();
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $branch = $this->input->post('branch');

        $data = [
            'name' => $name,
            'email' => $email,
            'branch' => $branch
        ];

        $res = $this->l->checkuser($data);

        if($res)
        {
            // redirect(base_url(''))
            echo "Success";
        }else{
            echo "Invalid";
        }

    }
}



?>