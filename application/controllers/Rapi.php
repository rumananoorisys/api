<?php
class Rapi extends CI_Controller{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Rapi_model','r');
    }
    public function insertrecord()
    {   
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $branch = $this->input->post('branch');
        
        
        $this->form_validation->set_rules('name','Name','required|alpha');
        $this->form_validation->set_rules('email','Email','required');
        $this->form_validation->set_rules('branch','Branch','required');

        if($this->form_validation->run()){
            $data = [
                'name' => $name,
                'email' => $email,
                'branch' => $branch
            ];
              
             $res = $this->r->addrecord($data);
             
             if($res==true)
             {
                 $response = [
                     'status' => 'Success ',
                     'message'=> 'Data inserted successfully!!!'
                 ];
             }else
             {
                 $response = [
                     'status' => 'Error',
                     'message' => 'Try Again????????'
                 ];
             }
    
        }else{
            $response = [
                'status'=>'Error',
                'message' => 'Invalid Value'
            ];
        }
                 echo json_encode($response);
    }

    public function showsingle()
    {
        $id = $this->input->post('id');
        $res = $this->r->showdata($id);
        echo json_encode($res);
    }

    public function deleteRecord()
    {
        $id = $this->input->post('id');

        $res = $this->r->deleteData($id);

        echo json_encode($res);
    }

    public function getdata()
    {
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $branch = $this->input->post('branch');

        $res = $this->r->getinfo($id);

        $data = [
            'name'=> isset($name)?$name:$res['name'],
            'email'=> isset($email)?$email:$res['email'],
            'branch'=> isset($branch)?$branch:$res['branch']
        ];

        $result = $this->r->updateInfo($data,$id);
         
        echo json_encode($result);



    }

}

?>