<?php
class Practice_model extends CI_Model
{
    public function checkInfo($data)
    {
        return $this->db->where('name',$data['name'])
        ->where('email',$data['email'])
        ->where('branch',$data['branch'])
        ->get('detail')
        ->row_array();
    }

    public function getTable()
    {
        return $this->db->select('*')
        ->get('detail')
        ->result_array();
    }

    public function getData($data)
    {
        return $this->db->insert('detail',$data);
    }

    public function deletRecord($id)
    {
       return $this->db->where('id',$id)
       ->delete('detail');
    }

    public function editRecord($id)
    {
        return $this->db->select('*')
        ->where('id',$id)
        ->get('detail')
        ->row_array();
    }

    public function updateData($data,$id)
    {
        return $this->db->set($data)
        ->where('id',$id)
        ->update('detail');
    }
}
?>