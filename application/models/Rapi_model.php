<?php
class Rapi_model extends CI_Model{
    public function addRecord($data){
        return $this->db->insert('detail',$data);
    }

    public function showdata($id)
    {
        return $this->db->select('*')
        ->where('id',$id)
        ->get('detail')
        ->row();
    }


    public function deleteData($id)
    {
        return $this->db->where('id',$id)
        ->delete('detail');
    }

    public function getinfo($id)
    {
        return $this->db->select('*')
        ->where('id',$id)
        ->get('detail')
        ->row_array();
    }

    public function updateInfo($data,$id)
    {
        return $this->db->set($data)
        ->where('id',$id)
        ->update('detail');
    }
}
?>